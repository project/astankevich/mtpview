#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Window.h>
#include <OutlineListView.h>
#include <FilePanel.h>
#include <Messenger.h>
#include <Notification.h>
#include <libmtp.h>


class FileItem: public BStringItem
{
public:
	FileItem(LIBMTP_mtpdevice_t * device, LIBMTP_devicestorage_t * storage, int id, BString filename, bool isFile, int depth);
	inline int fileId(){return fId;}
	inline bool isFile(){return fIsFile;}
	inline bool isFolder(){return !fIsFile;}
	inline LIBMTP_mtpdevice_t * device(){return this->fDevice;}	
	inline LIBMTP_devicestorage_t * storage() {return this->fStorage;}
	void SetExpanded(bool expanded);
private:
	int fId;
	bool fIsFile;
	LIBMTP_mtpdevice_t * fDevice;
	LIBMTP_devicestorage_t *fStorage;
};

class MainWindow : public BWindow
{
public:
	MainWindow(void);
	void		MessageReceived(BMessage *msg);
	inline BOutlineListView	* listView(){return this->fListView;};
	void showErrorAlert(const char * message);
	BNotification * uploadDownloadNotification;
private:
	BOutlineListView	*fListView;
	BButton *download;
	BButton *upload;
	BFilePanel* openPanel;
	BFilePanel* savePanel;
	
	BMessenger * messenger;
	
	FileItem * selectedItem;
};

#endif
