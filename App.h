#ifndef APP_H
#define APP_H

#include <Application.h>

class App : public BApplication
{
public:
	App(void);
	void AppActivated(bool active);
	void ReadyToRun();
};

#endif
