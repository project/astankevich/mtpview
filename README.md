## Description
Haiku's app to show a MTP-compilant device context and download/upload selected file to/from choosed location.

Note: Skip Android folder on device for perfomance.

![Screenshot](screenshots/screenshot1.png)

## Compiling
Required: libmtp_devel

Run in Terminal:
make

Alternative way is Paladin. Open mtpview.pld. Choose Build->Make project. 

## Packaging
Run in Terminal:
sh create_package.sh
