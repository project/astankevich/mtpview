#!/usr/bin/sh

#Making package dirs
mkdir -p pkg/apps
mkdir -p pkg/data/deskbar/menu/Applications

#Update binary
rm -f pkg/apps/mtpview
cp mtpview pkg/apps/mtpview
chmod +x pkg/apps/mtpview

#Make menu stortcut
pushd pkg/data/deskbar/menu/Applications
rm -f mtpview
ln -s -r ../../../../apps/mtpview mtpview
popd

#Create package
pushd pkg
package create -C ./ ../mtpview.hpkg 
popd
